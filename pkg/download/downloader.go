package download

import (
	"bitbucket.org/iamnotjustice/gget/pkg/console"
	"fmt"
	"io"
	"net/http"
	"os"
	"path"
	"path/filepath"
	"strconv"
	"sync"
	"time"
)

const (
	NotStarted = iota
	Started
	Finished
	Error
)

type Downloader struct {
	wc             *ProgressCounter
	wg             *sync.WaitGroup
	fileURL        string
	filename       string
	downloadStatus int
	path           string
	duration       time.Duration
}

func NewDownloader(filename, path, url string, wg *sync.WaitGroup) *Downloader {
	return &Downloader{
		wc:             &ProgressCounter{},
		wg:             wg,
		fileURL:        url,
		downloadStatus: NotStarted,
		path:           path,
		filename:       filename,
	}
}

func (d Downloader) CurrentProgress() string {
	switch d.downloadStatus {
	case NotStarted:
		return ""
	case Started:
		return fmt.Sprintf("%s downloading... %s out of %s bytes complete (%d %%) ",
			d.filename,
			humanizeSize(d.wc.bytesDownloaded),
			humanizeSize(d.wc.FileSize),
			((100 * d.wc.bytesDownloaded) / d.wc.FileSize),
		)
	case Finished:
		return fmt.Sprintf("%s download finished ",
			d.filename,
		)
	case Error:
		return fmt.Sprintf("%s download error ",
			d.filename,
		)
	default:
		return "unknown download status"
	}
}

func (d Downloader) result() string {
	switch d.downloadStatus {
	case Finished:
		return fmt.Sprintf("%s downloaded to %s folder, total time: %v", d.filename, d.path, d.duration.String())
	case Error:
		return fmt.Sprintf("%s download failed", d.filename)
	default:
		return "unknown download status"
	}
}

func (d *Downloader) GetStatus() int {
	return d.downloadStatus
}

func (d *Downloader) DownloadFile() error {
	startTime := time.Now()
	defer d.wg.Done()
	if d.filename == "" {
		d.filename = path.Base(d.fileURL)
	}

	os.MkdirAll(d.path, os.ModePerm)

	newPath := filepath.Join(d.path, d.filename)
	newFile, err := os.Create(newPath + ".download")
	if err != nil {
		d.downloadStatus = Error
		return err
	}
	defer newFile.Close()

	resp, err := http.Get(d.fileURL)
	if err != nil {
		d.downloadStatus = Error
		return err
	}
	defer resp.Body.Close()

	contentLenght := resp.Header.Get("Content-Length")
	fileSizeInBytes, err := strconv.ParseUint(contentLenght, 10, 64)
	if err != nil {
		d.downloadStatus = Error
		return err
	}

	d.wc.FileSize = fileSizeInBytes
	d.downloadStatus = Started
	_, err = io.Copy(newFile, io.TeeReader(resp.Body, d.wc))
	if err != nil {
		d.downloadStatus = Error
		return err
	}

	err = os.Rename(newPath+".download", d.path+d.filename)
	if err != nil {
		d.downloadStatus = Error
		return err
	}
	d.downloadStatus = Finished
	d.duration = time.Since(startTime)
	return nil
}

func humanizeSize(b uint64) string {
	const unit = 1000
	if b < unit {
		return fmt.Sprintf("%d B", b)
	}

	div := int64(unit)
	exp := 0

	for n := b / unit; n >= unit; n /= unit {
		div *= unit
		exp++
	}

	return fmt.Sprintf("%.1f %cB",
		float64(b)/float64(div), "kMGTPE"[exp])
}

func Results(downloaders []*Downloader) {
	console.Clear()
	for _, v := range downloaders {
		fmt.Println(v.CurrentProgress())
	}
	for _, v := range downloaders {
		fmt.Println(v.result())
	}
}
