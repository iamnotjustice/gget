package download

import (
	"fmt"
	"time"

	"bitbucket.org/iamnotjustice/gget/pkg/console"
)

type ProgressCounter struct {
	FileSize        uint64
	bytesDownloaded uint64
}

func (wc *ProgressCounter) Write(p []byte) (int, error) {
	n := len(p)
	wc.bytesDownloaded += uint64(n)
	return n, nil
}

func CheckProgress(downloaders []*Downloader) {
	for {
		console.Clear()
		for _, v := range downloaders {
			if v.GetStatus() != NotStarted {
				fmt.Println(v.CurrentProgress())
			}
		}
		time.Sleep(500 * time.Millisecond)
	}
}
