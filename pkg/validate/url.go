package validate

import (
	"errors"
	"net/url"
)

func ValidateURLs(urls []string) error {
	if len(urls) == 0 {
		return errors.New("empty arguments list")
	}
	for _, v := range urls {
		_, err := url.ParseRequestURI(v)
		if err != nil {
			return err
		}
	}
	return nil
}
