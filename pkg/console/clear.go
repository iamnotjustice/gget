package console

import (
	"os"
	"os/exec"
	"runtime"
)

var clearfuncs map[string]func()

func init() {
	clearfuncs = make(map[string]func())
	clearfuncs["linux"] = func() {
		cmd := exec.Command("clear")
		cmd.Stdout = os.Stdout
		cmd.Run()
	}
	clearfuncs["windows"] = func() {
		cmd := exec.Command("cmd", "/c", "cls")
		cmd.Stdout = os.Stdout
		cmd.Run()
	}
}

func Clear() {
	value, ok := clearfuncs[runtime.GOOS]
	if ok {
		value()
	} else {
		panic("platform is unsupported")
	}
}
