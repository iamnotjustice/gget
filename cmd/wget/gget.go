package main

import (
	"flag"
	"fmt"
	"sync"

	"bitbucket.org/iamnotjustice/gget/pkg/download"
	"bitbucket.org/iamnotjustice/gget/pkg/validate"
)

func main() {
	var path, out string
	flag.StringVar(&path, "path", "", "path to save files")
	flag.StringVar(&out, "out", "", "filename of downloaded file")
	flag.Parse()
	fileURLs := flag.Args()

	if out != "" && len(fileURLs) > 1 {
		fmt.Print("cannot use -out flag with multiple files")
		return
	}

	err := validate.ValidateURLs(fileURLs)
	if err != nil {
		fmt.Printf("error downloading files: %+v \n", err)
		return
	}

	wg := &sync.WaitGroup{}
	wg.Add(len(fileURLs))
	downloaders := make([]*download.Downloader, len(fileURLs))
	for i := range downloaders {
		downloaders[i] = download.NewDownloader(out, path, fileURLs[i], wg)
		go downloaders[i].DownloadFile()
	}

	go download.CheckProgress(downloaders)
	wg.Wait()
	download.Results(downloaders)
}
