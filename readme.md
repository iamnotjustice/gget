# wget written in golang

gget [OPTIONS] [URL]...

Options:
    -path=PATH      save file to path
    -out=FILENAME   save downloaded file with name

## HOW TO BUILD AND USE

$ make build

$ ./gget https://golang.org/doc/gopher/bumper.png https://dl.google.com/go/go1.12.4.darwin-amd64.pkg https://dl.google.com/go/go1.12.4.linux-amd64.tar.gz https://golang.org/doc/gopher/biplane.jpg

$ ./gget -path ./here/ https://golang.org/doc/gopher/bumper.png https://dl.google.com/go/go1.12.4.darwin-amd64.pkg https://dl.google.com/go/go1.12.4.linux-amd64.tar.gz https://golang.org/doc/gopher/biplane.jpg

$ ./gget -out gopher.jpg -path ./here/ https://golang.org/doc/gopher/biplane.jpg 